package com.gojek;

import org.junit.Test;

import static org.junit.Assert.*;

public class TaskListTest {
    @Test
    public void testGetAllTask() {
        TaskList taskList = new TaskList();
        String expectedTask = "1. Do dishes [DONE]\n2. Learn Java [NOT DONE]\n3. Learn TDD [NOT DONE]";

        String actual = taskList.getAllTask();

        assertEquals(expectedTask, actual);
    }

    @Test
    public void testGetTaskById() {
        TaskList taskList = new TaskList();
        String expectedTask = "1. Do dishes [DONE]";

        String actual = taskList.getTask("1");

        assertEquals(expectedTask, actual);
    }

    @Test
    public void testUpdateTask(){
        TaskList taskList = new TaskList();
        String expectedTask = "2. Learn Java [DONE]";

        taskList.markTaskDone("2");
        String actual = taskList.getTask("2");

        assertEquals(expectedTask, actual);
    }
}