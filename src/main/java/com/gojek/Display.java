package com.gojek;

import java.util.Scanner;

public class Display {
    private Scanner scan = new Scanner(System.in);
    private TaskList taskList = new TaskList();
    private Task task;

    public Display() {
        int input;

        do {
            printMenu();
            input = scan.nextInt();
            scan.nextLine();
            switch (input){
                case 1:
                    System.out.println(taskList.getAllTask());
                    break;
                case 2:
                    taskList.displayUpdateTask();
                    System.out.println(taskList.getAllTask());
                    break;
                default:
                    break;
            }
        }while (input != 3);
    }
    private static void printMenu(){
        System.out.println("To Do List");
        System.out.println("=======================");
        System.out.println("1. Get all To Do List");
        System.out.println("2. Update To Do List");
        System.out.println("3. Exit");
        System.out.println("=======================");
    }
}
