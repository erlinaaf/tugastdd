package com.gojek;

public class Task {
    private String name;
    private String status;
    private String id;

    public Task(String id, String name, String status) {
        this.id = id;
        this.name = name;
        this.status = status;
    }

    public String getId() {
        return this.id;
    }

    public void markTaskDone(){
        this.status = "DONE";
    }

    @Override
    public String toString() {
        return id + ". " + name + " [" + status + "]";
    }
}
