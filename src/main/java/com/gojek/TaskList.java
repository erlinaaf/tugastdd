package com.gojek;

import java.util.ArrayList;
import java.util.Scanner;

public class TaskList
{
    private Scanner scan = new Scanner(System.in);
    private ArrayList<Task> taskList;

    public TaskList() {
        taskList = new ArrayList<>();
        this.taskList.add(new Task("1", "Do dishes", "DONE"));
        this.taskList.add(new Task("2", "Learn Java", "NOT DONE"));
        this.taskList.add(new Task("3", "Learn TDD", "NOT DONE"));
    }

    public String getAllTask() {
        String allTask = "";
        for (int i = 0; i < taskList.size(); i++) {
            Task task = taskList.get(i);
            allTask = allTask + task.toString();
            if (i != taskList.size() - 1) {
                allTask = allTask + "\n";
            }
        }
        return allTask;
    }

    public String getTask(String id) {
        for(Task task : this.taskList) {
            if(task.getId().equals(id)){
                return task.toString();
            }
        }
        return null;
    }

    public void markTaskDone(String id) {
        for(Task task : this.taskList) {
            if(task.getId().equals(id)){
                task.markTaskDone();
            }
        }
    }

    public void displayUpdateTask(){
        System.out.println("Which task you want to update?");
        String id = scan.nextLine();
        markTaskDone(id);
    }
}
